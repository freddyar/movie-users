package com.livecron.users.api.model;

/**
 * @author Santiago Mamani
 */
public enum AccountState {
    ACTIVATED,
    DEACTIVATED
}
