package com.livecron.users.service.service;

import com.livecron.employees.api.input.EmployeeCreateInput;
import com.livecron.users.service.client.employees.model.Employee;
import com.livecron.users.service.client.employees.service.SystemEmployeeService;
import com.livecron.users.service.exception.AccountNotFoundException;
import com.livecron.users.service.model.domain.Account;
import com.livecron.users.service.model.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeCreateService {

    private EmployeeCreateInput employeeCreateInput;

    private Long accountId;

    private Account account;

    private Employee employee;

    @Autowired
    private SystemEmployeeService systemEmployeeService;

    @Autowired
    private AccountRepository accountRepository;

    public void execute() {
        account = findAccount(accountId);
        if (null != account) {
            employee = systemEmployeeService.createEmployee(employeeCreateInput);
        }
    }

    private Account findAccount(Long accountId) {
        return accountRepository.findById(accountId)
                .orElseThrow(() -> new AccountNotFoundException("Account not found to accountId: " + accountId));
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }


    public EmployeeCreateInput getEmployeeCreateInput() {
        return employeeCreateInput;
    }

    public void setEmployeeCreateInput(EmployeeCreateInput employeeCreateInput) {
        this.employeeCreateInput = employeeCreateInput;
    }

    public Employee getEmployee() {
        return employee;
    }
}
