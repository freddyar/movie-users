package com.livecron.users.service.model.repository;

import com.livecron.users.api.model.AccountState;
import com.livecron.users.service.model.domain.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

/**
 * @author Santiago Mamani
 */
public interface AccountRepository extends JpaRepository<Account, Long> {
    Optional<Account> findByEmailAndState(String email, AccountState state);

    @Query("select item from Account item where item.email =:email or item.state =:state")
    List<Account> findAllAccount(@Param("email") String mail,
                                 @Param("state") AccountState state);
}
