package com.livecron.users.service.controller;

import com.livecron.users.api.input.TeacherCreateInput;
import com.livecron.users.service.model.domain.Teacher;
import com.livecron.users.service.service.TeacherCreateService;
import com.livecron.users.service.service.TeacherReadByIdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Santiago Mamani
 */
@RequestMapping(value = "/teachers")
@RequestScope
@RestController
public class TeacherController {

    @Autowired
    private TeacherCreateService teacherCreateService;

    @Autowired
    private TeacherReadByIdService teacherReadByIdService;

    @RequestMapping(method = RequestMethod.POST)
    public Teacher createTeacher(@RequestBody TeacherCreateInput input) {
        teacherCreateService.setInput(input);
        teacherCreateService.execute();

        return teacherCreateService.getTeacher();
    }

    @RequestMapping(value = "/{teacherId}", method = RequestMethod.GET)
    public Teacher readTeacher(@PathVariable("teacherId") Long teacherId) {
        teacherReadByIdService.setTeacherId(teacherId);
        teacherReadByIdService.execute();

        return teacherReadByIdService.getTeacher();
    }
}
