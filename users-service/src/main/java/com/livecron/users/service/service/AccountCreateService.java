package com.livecron.users.service.service;

import com.livecron.users.api.input.AccountCreateInput;
import com.livecron.users.service.model.domain.Account;
import com.livecron.users.service.model.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Santiago Mamani
 */
@Service
public class AccountCreateService {

    private AccountCreateInput input;

    private Account account;

    @Autowired
    private AccountRepository accountRepository;

    public void execute() {
        Account instance = composeAccountInstance(input);
        account = accountRepository.save(instance);
    }

    private Account composeAccountInstance(AccountCreateInput input) {
        Account instance = new Account();
        instance.setEmail(input.getEmail());
        instance.setState(input.getState());

        return instance;
    }

    public void setInput(AccountCreateInput input) {
        this.input = input;
    }

    public Account getAccount() {
        return account;
    }
}
