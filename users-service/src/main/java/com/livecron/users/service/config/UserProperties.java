package com.livecron.users.service.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @author Santiago Mamani
 */
@Configuration
@PropertySource("classpath:/configuration/user-service.properties")
public class UserProperties {

    @Value("${users.allowed.age:15}")
    private Long age;

    public Long getAge() {
        return age;
    }

    public void setAge(Long age) {
        this.age = age;
    }
}
