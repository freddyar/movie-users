package com.livecron.users.service.controller;


import com.livecron.employees.api.input.EmployeeCreateInput;
import com.livecron.users.service.client.employees.model.Employee;
import com.livecron.users.service.service.EmployeeCreateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

@RequestMapping(value = "/employees")
@RequestScope
@RestController
public class EmployeeController {

    @Autowired
    private EmployeeCreateService employeeCreateService;

    @RequestMapping(
            value = "/create",
            method = RequestMethod.POST
    )
    public Employee createEmployee(@RequestBody EmployeeCreateInput employeeInput) {
        employeeCreateService.setAccountId(employeeInput.getAccoutId());
        employeeCreateService.setEmployeeCreateInput(employeeInput);
        employeeCreateService.execute();

        return employeeCreateService.getEmployee();
    }
}
