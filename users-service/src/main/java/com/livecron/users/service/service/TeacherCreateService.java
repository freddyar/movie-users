package com.livecron.users.service.service;

import com.livecron.users.api.input.TeacherCreateInput;
import com.livecron.users.api.model.AccountState;
import com.livecron.users.service.config.UserProperties;
import com.livecron.users.service.model.domain.Account;
import com.livecron.users.service.model.domain.Teacher;
import com.livecron.users.service.model.repository.AccountRepository;
import com.livecron.users.service.model.repository.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @author Santiago Mamani
 */
@Service
public class TeacherCreateService {

    private TeacherCreateInput input;

    private Teacher teacher;

    @Autowired
    private UserProperties userProperties;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private TeacherRepository teacherRepository;

    public void execute() {
        Account account = composeAccountInstance();
        account = accountRepository.save(account);

        Teacher instance = composeTeacherInstance(account);
        teacher = teacherRepository.save(instance);
    }

    private Account composeAccountInstance() {
        AccountState state = readState();
        Account account = new Account();
        account.setEmail(input.getEmail());
        account.setState(state);

        return account;
    }

    private AccountState readState() {
        return input.getAge() >= userProperties.getAge() ? AccountState.ACTIVATED : AccountState.DEACTIVATED;
    }

    private Teacher composeTeacherInstance(Account account) {
        Teacher instance = new Teacher();
        instance.setFirstName(input.getFirstName());
        instance.setLastName(input.getLastName());
        instance.setPassword(input.getPassword());
        instance.setProfession(input.getProfession());
        instance.setActive(Boolean.TRUE);
        instance.setCreatedDate(new Date());
        instance.setAccount(account);

        return instance;
    }

    public void setInput(TeacherCreateInput input) {
        this.input = input;
    }

    public Teacher getTeacher() {
        return teacher;
    }
}
