package com.livecron.users.service.service;

import com.livecron.users.service.exception.TeacherNotFoundException;
import com.livecron.users.service.model.domain.Teacher;
import com.livecron.users.service.model.repository.TeacherRepository;
import org.springframework.stereotype.Service;

/**
 * @author Santiago Mamani
 */
@Service
public class TeacherReadByIdService extends AbstractTeacherReadService {

    private Long teacherId;

    @Override
    protected Teacher findTeacher(TeacherRepository repository) {
        return repository.findById(teacherId)
                .orElseThrow(() -> new TeacherNotFoundException(
                        "Unable locate teacher with teacherId: " + teacherId
                ));
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }
}
