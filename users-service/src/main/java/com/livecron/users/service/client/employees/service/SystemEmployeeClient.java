package com.livecron.users.service.client.employees.service;

import com.livecron.employees.api.input.EmployeeCreateInput;
import com.livecron.users.service.client.employees.model.Employee;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "employees-service")
public interface SystemEmployeeClient {

    @RequestMapping(
            value = "/employees",
            method = RequestMethod.POST
    )
    Employee createEmployee(@RequestBody EmployeeCreateInput employeeInput);
}
