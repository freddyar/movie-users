package com.livecron.users.service.service;

import com.livecron.users.service.model.domain.Teacher;
import com.livecron.users.service.model.repository.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Santiago Mamani
 */
abstract class AbstractTeacherReadService {

    private Teacher teacher;

    @Autowired
    private TeacherRepository repository;

    public void execute() {
        teacher = findTeacher(repository);
    }

    protected abstract Teacher findTeacher(TeacherRepository repository);

    public Teacher getTeacher() {
        return teacher;
    }
}
