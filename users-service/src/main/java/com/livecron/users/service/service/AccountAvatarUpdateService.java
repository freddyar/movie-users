package com.livecron.users.service.service;

import com.livecron.users.service.client.binaries.model.Binary;
import com.livecron.users.service.client.binaries.service.SystemBinaryService;
import com.livecron.users.service.exception.AccountNotFoundException;
import com.livecron.users.service.model.domain.Account;
import com.livecron.users.service.model.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Santiago Mamani
 */
@Service
public class AccountAvatarUpdateService {

    private Long accountId;

    private MultipartFile file;

    private Account account;

    @Autowired
    private SystemBinaryService systemBinaryService;

    @Autowired
    private AccountRepository accountRepository;

    public void execute() {
        account = findAccount(accountId);
        if (null == account.getAvatarId()) {
            Binary binary = systemBinaryService.uploadBinary(file);
            account.setAvatarId(binary.getId());
            account = accountRepository.save(account);
        }
    }

    private Account findAccount(Long accountId) {
        return accountRepository.findById(accountId)
                .orElseThrow(() -> new AccountNotFoundException("Account not found to accountId: " + accountId));
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public Account getAccount() {
        return account;
    }
}
